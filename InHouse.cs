﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BFM1_Task
{
    public class InHouse : Part
    {
        public override int PartID { get ; set; }
        public override int MachineID { get; set; }

        public InHouse(string name, decimal price, int inStock,
            int min, int max, int machineID) : base(name, price, inStock, min, max)
        {
            MachineID = machineID;
            PartID = IMS.Increment_Part_ID();
        }
    }
}
