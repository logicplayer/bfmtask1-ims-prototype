﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BFM1_Task
{
    public class Inventory
    {
        public List<Product> Products { get; set; } = new List<Product>();
        public List<Part> AllParts { get; set; } = new List<Part>();

        public void AddProduct(Product product)
        {
            Products.Add(product);
        }

        public void AddPart(Part part)
        {
            AllParts.Add(part);
        }

        public bool RemoveProduct(int productID)
        {
            var productIndex = Products.FindIndex(p => p.ProductID == productID);

            Products.RemoveAt(productIndex);

            return true; // placeholder
        }

        public Product LookupProduct(int productIndex)
        {
            var product =
                from value in Products
                where productIndex == value.ProductID
                select value;
            return (Product)product;
        }

        public void Updateproduct(int productID, ref Product product)
        {
            this.RemoveProduct(productID);
            Products.Add(product);
        }

        public bool DeletePart(Part part)
        {
            return AllParts.Remove(part);
        }

        public Part LookupPart(int partID)
        {
            return AllParts.Find(p => p.PartID == partID);
        }

        public void UpdatePart(int partID, ref Part part)
        {
            RemoveProduct(partID);
            AllParts.Add(part);
        }
    }
}
