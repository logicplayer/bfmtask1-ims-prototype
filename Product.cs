﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BFM1_Task
{
    public class Product : Part
    {
        public int ProductID { get; private set; }

        public Product(string name, decimal price, int inStock, int min, int max)
            : base(name, price, inStock, min, max)
        {
            ProductID = IMS.Increment_Product_ID();
        }

        public List<Part> AssociatedParts = new List<Part>();

        public void AddAssociatedPart(Part part)
        {
            AssociatedParts.Add(part);
        }

        public bool RemoveAssociatedPart(int part)
        {
            try
            {
                AssociatedParts.RemoveAt(part);
            }
            catch(Exception)
            {
                return false;
            }

            return true;
        }

        public Part LookupAssociatedPart(int partID)
        {
            return AssociatedParts.Find(p => p.PartID == partID);
        }
    }
}
