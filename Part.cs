﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BFM1_Task
{
    public abstract class Part
    {
        public virtual int PartID {get; set;}
        public virtual int MachineID { get; set; }
        public virtual string CompanyName { get; set; }

        public Part(string name, decimal price, int inStock,
            int min, int max)
        {
            Name = name;
            Price = price;
            InStock = inStock;
            Min = min;
            Max = max;
        }

        public string Name { get; set; }
        public decimal Price { get; set; }
        public int InStock { get; set; }
        public int Min { get; set; }
        public int Max { get; set; }
    }
}
