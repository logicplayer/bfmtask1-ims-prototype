﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BFM1_Task
{
    public class Outsourced : Part
    {
        public override string CompanyName { get; set; }
        public override int PartID { get; set; }

        public Outsourced(string name, decimal price, int inStock,
            int min, int max, string companyName) : base(name, price, inStock, min, max)
        {
            CompanyName = companyName;
            PartID = IMS.Increment_Part_ID();
        }
    }
}
