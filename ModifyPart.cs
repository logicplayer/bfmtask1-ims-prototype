﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BFM1_Task
{
    public partial class ModifyPart : Form
    {
        private Part selectedPart;

        public ModifyPart(ref Part part)
        {
            InitializeComponent();
            selectedPart = part;
            boxID.Text = selectedPart.PartID.ToString();
            boxName.Text = selectedPart.Name;
            boxInventory.Text = selectedPart.InStock.ToString();
            boxPriceCost.Text = selectedPart.Price.ToString();
            boxMax.Text = selectedPart.Max.ToString();
            boxMin.Text = selectedPart.Min.ToString();
            if (selectedPart is InHouse)
            {
                try
                {
                    boxMachineCompany.Text = selectedPart.MachineID.ToString();
                }
                catch (Exception)
                {
                }
            }
            else if (selectedPart is Outsourced)
            {
                try
                {
                    boxMachineCompany.Text = selectedPart.CompanyName;
                }
                catch (Exception)
                {
                }
            }
        }

        private void OnFormClosing(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void radOutsourced_CheckedChanged(object sender, EventArgs e)
        {
            if (radInHouse.Checked)
            {
                lblMachineCompany.Text = "Machine ID";
            }
            else if (radOutsourced.Checked)
            {
                lblMachineCompany.Text = "Company Name";
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (radInHouse.Checked && !radOutsourced.Checked)
            {
                selectedPart.Name = boxName.Text;
                selectedPart.Price = decimal.Parse(boxPriceCost.Text);
                selectedPart.InStock = int.Parse(boxInventory.Text);
                selectedPart.Min = int.Parse(boxMin.Text);
                selectedPart.Max = int.Parse(boxMax.Text);
                selectedPart.MachineID = int.Parse(boxMachineCompany.Text);
            }
            else if (!radInHouse.Checked && radOutsourced.Checked)
            {
                selectedPart.Name = boxName.Text;
                selectedPart.Price = decimal.Parse(boxPriceCost.Text);
                selectedPart.InStock = int.Parse(boxInventory.Text);
                selectedPart.Min = int.Parse(boxMin.Text);
                selectedPart.Max = int.Parse(boxMax.Text);
                selectedPart.CompanyName = boxMachineCompany.Text;
            }

            Controller.inventory.UpdatePart(selectedPart.PartID, ref selectedPart);
        }
    }
}
