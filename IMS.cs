﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BFM1_Task
{
    public static class IMS
    {
        public static int NextPartID { get; set; }
        public static int NextProductID { get; set; }

        static IMS()
        {
        }

        public static int Increment_Part_ID()
        {
            NextPartID++;
            return NextPartID;
        }

        public static int Increment_Product_ID()
        {
            NextProductID++;
            return NextProductID;
        }

        public static ListViewItem CreateListViewItem(Part part)
        {
            var item = new ListViewItem();
            item.SubItems.Add(part.PartID.ToString());
            item.SubItems.RemoveAt(0);
            item.SubItems.Add(part.Name);
            item.SubItems.Add(part.Price.ToString());
            item.SubItems.Add(part.InStock.ToString());
            item.SubItems.Add(part.Min.ToString());
            item.SubItems.Add(part.Max.ToString());
            // MessageBox.Show(item.SubItems[0].Text, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return item;
        }

        public static ListViewItem CreateListViewItem(Product product)
        {
            var item = new ListViewItem();
            item.SubItems.Add(product.ProductID.ToString());
            item.SubItems.RemoveAt(0);
            item.SubItems.Add(product.Name);
            item.SubItems.Add(product.Price.ToString());
            item.SubItems.Add(product.InStock.ToString());
            item.SubItems.Add(product.Min.ToString());
            item.SubItems.Add(product.Max.ToString());

            return item;
        }

        public static void UpdateDataSource(ref ListView listView, List<Part> parts)
        {
            listView.Clear();
            listView.Columns.Add("Part ID");
            listView.Columns.Add("Name");
            listView.Columns.Add("Price");
            listView.Columns.Add("Inventory");
            listView.Columns.Add("Min");
            listView.Columns.Add("Max");
            listView.View = View.Details;
            var items = new List<ListViewItem>();

            foreach (var part in parts)
            {
                items.Add(IMS.CreateListViewItem(part));
            }

            foreach (var item in items)
            {
                listView.Items.Add(item);
            }

            listView.Refresh();
            listView.Update();
        }

        public static void UpdateDataSource(ref ListView listView, List<Product> products)
        {
            listView.Clear();
            listView.Columns.Add("Product ID");
            listView.Columns.Add("Name");
            listView.Columns.Add("Price");
            listView.Columns.Add("Inventory");
            listView.Columns.Add("Min");
            listView.Columns.Add("Max");
            listView.View = View.Details;

            var items = new List<ListViewItem>();

            foreach (var product in products)
            {
                items.Add(IMS.CreateListViewItem(product));
            }

            foreach (var item in items)
            {
                listView.Items.Add(item);
            }

            listView.Refresh();
            listView.Update();
        }
    }
}
