﻿namespace BFM1_Task
{
    partial class ModifyPart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PartLabel = new System.Windows.Forms.Label();
            this.radInHouse = new System.Windows.Forms.RadioButton();
            this.radOutsourced = new System.Windows.Forms.RadioButton();
            this.radPanel = new System.Windows.Forms.Panel();
            this.lblID = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblInventory = new System.Windows.Forms.Label();
            this.lblPriceCost = new System.Windows.Forms.Label();
            this.lblMax = new System.Windows.Forms.Label();
            this.lblMin = new System.Windows.Forms.Label();
            this.lblMachineCompany = new System.Windows.Forms.Label();
            this.boxID = new System.Windows.Forms.TextBox();
            this.boxName = new System.Windows.Forms.TextBox();
            this.boxInventory = new System.Windows.Forms.TextBox();
            this.boxPriceCost = new System.Windows.Forms.TextBox();
            this.boxMax = new System.Windows.Forms.TextBox();
            this.boxMachineCompany = new System.Windows.Forms.TextBox();
            this.boxMin = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.radPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // PartLabel
            // 
            this.PartLabel.AutoSize = true;
            this.PartLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PartLabel.Location = new System.Drawing.Point(12, 9);
            this.PartLabel.Name = "PartLabel";
            this.PartLabel.Size = new System.Drawing.Size(121, 25);
            this.PartLabel.TabIndex = 0;
            this.PartLabel.Text = "Modify Part";
            // 
            // radInHouse
            // 
            this.radInHouse.AutoSize = true;
            this.radInHouse.Checked = true;
            this.radInHouse.Location = new System.Drawing.Point(3, 17);
            this.radInHouse.Name = "radInHouse";
            this.radInHouse.Size = new System.Drawing.Size(68, 17);
            this.radInHouse.TabIndex = 1;
            this.radInHouse.TabStop = true;
            this.radInHouse.Text = "In-House";
            this.radInHouse.UseVisualStyleBackColor = true;
            // 
            // radOutsourced
            // 
            this.radOutsourced.AutoSize = true;
            this.radOutsourced.Location = new System.Drawing.Point(117, 17);
            this.radOutsourced.Name = "radOutsourced";
            this.radOutsourced.Size = new System.Drawing.Size(80, 17);
            this.radOutsourced.TabIndex = 2;
            this.radOutsourced.TabStop = true;
            this.radOutsourced.Text = "Outsourced";
            this.radOutsourced.UseVisualStyleBackColor = true;
            this.radOutsourced.CheckedChanged += new System.EventHandler(this.radOutsourced_CheckedChanged);
            // 
            // radPanel
            // 
            this.radPanel.Controls.Add(this.radInHouse);
            this.radPanel.Controls.Add(this.radOutsourced);
            this.radPanel.Location = new System.Drawing.Point(269, 12);
            this.radPanel.Name = "radPanel";
            this.radPanel.Size = new System.Drawing.Size(200, 51);
            this.radPanel.TabIndex = 3;
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblID.Location = new System.Drawing.Point(139, 102);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(26, 20);
            this.lblID.TabIndex = 4;
            this.lblID.Text = "ID";
            this.lblID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(114, 144);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(51, 20);
            this.lblName.TabIndex = 5;
            this.lblName.Text = "Name";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblInventory
            // 
            this.lblInventory.AutoSize = true;
            this.lblInventory.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInventory.Location = new System.Drawing.Point(91, 186);
            this.lblInventory.Name = "lblInventory";
            this.lblInventory.Size = new System.Drawing.Size(74, 20);
            this.lblInventory.TabIndex = 6;
            this.lblInventory.Text = "Inventory";
            this.lblInventory.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblPriceCost
            // 
            this.lblPriceCost.AutoSize = true;
            this.lblPriceCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPriceCost.Location = new System.Drawing.Point(76, 228);
            this.lblPriceCost.Name = "lblPriceCost";
            this.lblPriceCost.Size = new System.Drawing.Size(89, 20);
            this.lblPriceCost.TabIndex = 7;
            this.lblPriceCost.Text = "Price / Cost";
            this.lblPriceCost.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblMax
            // 
            this.lblMax.AutoSize = true;
            this.lblMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMax.Location = new System.Drawing.Point(127, 270);
            this.lblMax.Name = "lblMax";
            this.lblMax.Size = new System.Drawing.Size(38, 20);
            this.lblMax.TabIndex = 8;
            this.lblMax.Text = "Max";
            this.lblMax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblMin
            // 
            this.lblMin.AutoSize = true;
            this.lblMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMin.Location = new System.Drawing.Point(268, 270);
            this.lblMin.Name = "lblMin";
            this.lblMin.Size = new System.Drawing.Size(34, 20);
            this.lblMin.TabIndex = 9;
            this.lblMin.Text = "Min";
            this.lblMin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblMachineCompany
            // 
            this.lblMachineCompany.AutoSize = true;
            this.lblMachineCompany.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMachineCompany.Location = new System.Drawing.Point(75, 312);
            this.lblMachineCompany.Name = "lblMachineCompany";
            this.lblMachineCompany.Size = new System.Drawing.Size(90, 20);
            this.lblMachineCompany.TabIndex = 10;
            this.lblMachineCompany.Text = "Machine ID";
            this.lblMachineCompany.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // boxID
            // 
            this.boxID.Location = new System.Drawing.Point(172, 101);
            this.boxID.Name = "boxID";
            this.boxID.ReadOnly = true;
            this.boxID.Size = new System.Drawing.Size(100, 20);
            this.boxID.TabIndex = 11;
            // 
            // boxName
            // 
            this.boxName.Location = new System.Drawing.Point(172, 144);
            this.boxName.Name = "boxName";
            this.boxName.Size = new System.Drawing.Size(100, 20);
            this.boxName.TabIndex = 12;
            // 
            // boxInventory
            // 
            this.boxInventory.Location = new System.Drawing.Point(172, 186);
            this.boxInventory.Name = "boxInventory";
            this.boxInventory.Size = new System.Drawing.Size(100, 20);
            this.boxInventory.TabIndex = 13;
            // 
            // boxPriceCost
            // 
            this.boxPriceCost.Location = new System.Drawing.Point(172, 228);
            this.boxPriceCost.Name = "boxPriceCost";
            this.boxPriceCost.Size = new System.Drawing.Size(100, 20);
            this.boxPriceCost.TabIndex = 14;
            // 
            // boxMax
            // 
            this.boxMax.Location = new System.Drawing.Point(171, 270);
            this.boxMax.Name = "boxMax";
            this.boxMax.Size = new System.Drawing.Size(51, 20);
            this.boxMax.TabIndex = 15;
            // 
            // boxMachineCompany
            // 
            this.boxMachineCompany.Location = new System.Drawing.Point(171, 312);
            this.boxMachineCompany.Name = "boxMachineCompany";
            this.boxMachineCompany.Size = new System.Drawing.Size(100, 20);
            this.boxMachineCompany.TabIndex = 17;
            // 
            // boxMin
            // 
            this.boxMin.Location = new System.Drawing.Point(308, 270);
            this.boxMin.Name = "boxMin";
            this.boxMin.Size = new System.Drawing.Size(51, 20);
            this.boxMin.TabIndex = 18;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(404, 346);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 22;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(308, 346);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 21;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // ModifyPart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(518, 394);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.boxMin);
            this.Controls.Add(this.boxMachineCompany);
            this.Controls.Add(this.boxMax);
            this.Controls.Add(this.boxPriceCost);
            this.Controls.Add(this.boxInventory);
            this.Controls.Add(this.boxName);
            this.Controls.Add(this.boxID);
            this.Controls.Add(this.lblMachineCompany);
            this.Controls.Add(this.lblMin);
            this.Controls.Add(this.lblMax);
            this.Controls.Add(this.lblPriceCost);
            this.Controls.Add(this.lblInventory);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblID);
            this.Controls.Add(this.radPanel);
            this.Controls.Add(this.PartLabel);
            this.Name = "ModifyPart";
            this.Text = "Modify Part";
            this.radPanel.ResumeLayout(false);
            this.radPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label PartLabel;
        private System.Windows.Forms.RadioButton radInHouse;
        private System.Windows.Forms.RadioButton radOutsourced;
        private System.Windows.Forms.Panel radPanel;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblInventory;
        private System.Windows.Forms.Label lblPriceCost;
        private System.Windows.Forms.Label lblMax;
        private System.Windows.Forms.Label lblMin;
        private System.Windows.Forms.Label lblMachineCompany;
        private System.Windows.Forms.TextBox boxID;
        private System.Windows.Forms.TextBox boxName;
        private System.Windows.Forms.TextBox boxInventory;
        private System.Windows.Forms.TextBox boxPriceCost;
        private System.Windows.Forms.TextBox boxMax;
        private System.Windows.Forms.TextBox boxMachineCompany;
        private System.Windows.Forms.TextBox boxMin;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
    }
}