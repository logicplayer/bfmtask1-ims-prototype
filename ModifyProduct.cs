﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BFM1_Task
{
    public partial class ModifyProduct : Form
    {
        private Product selectedProduct;
        public List<Part> AllCandidateParts { get; set; }

        public ModifyProduct(ref Product product)
        {
            InitializeComponent();
            selectedProduct = product;
            AllCandidateParts = Controller.inventory.AllParts;

            IMS.UpdateDataSource(ref CandidateParts, AllCandidateParts);

            IMS.UpdateDataSource(ref AssociatedParts, selectedProduct.AssociatedParts);

            boxID.Text = selectedProduct.ProductID.ToString();
            boxInventory.Text = selectedProduct.InStock.ToString();
            boxMax.Text = selectedProduct.Max.ToString();
            boxMin.Text = selectedProduct.Min.ToString();
            boxName.Text = selectedProduct.Name;
            boxPriceCost.Text = selectedProduct.Price.ToString();
        }

        private void OnFormClosing(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var selectedPart = AllCandidateParts.Find(p => p.PartID == int.Parse(CandidateParts.SelectedItems[0].SubItems[0].Text));
            selectedProduct.AssociatedParts.Add(selectedPart);
            IMS.UpdateDataSource(ref AssociatedParts, selectedProduct.AssociatedParts);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (selectedProduct.AssociatedParts.Count != 0)
            {
                var selectedPart = selectedProduct.AssociatedParts.Find(p => p.PartID == int.Parse(AssociatedParts.SelectedItems[0].SubItems[0].Text));
                selectedProduct.AssociatedParts.Remove(selectedPart);
            }
            IMS.UpdateDataSource(ref AssociatedParts, selectedProduct.AssociatedParts);
            AssociatedParts.Refresh();
            AssociatedParts.Update();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            selectedProduct.Max = int.Parse(boxMax.Text);
            selectedProduct.Min = int.Parse(boxMin.Text);
            selectedProduct.Price = decimal.Parse(boxPriceCost.Text);
            selectedProduct.InStock = int.Parse(boxInventory.Text);
            selectedProduct.Name = boxName.Text;

            Controller.inventory.Updateproduct(selectedProduct.ProductID, ref selectedProduct);

            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            var result =
                from value in AllCandidateParts
                where value.Name.Equals(txtSearch.Text, StringComparison.OrdinalIgnoreCase)
                select value;

            var results = result.ToList();

            IMS.UpdateDataSource(ref CandidateParts, results);
        }
    }
}
