﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BFM1_Task
{
    public partial class AddProduct : Form
    {
        public List<Part> AllCandidateParts { get; set; }
        public List<Part> CurrentAssociatedParts { get; set; }

        public AddProduct()
        {
            InitializeComponent();

            AllCandidateParts = Controller.inventory.AllParts;

            IMS.UpdateDataSource(ref CandidateParts, AllCandidateParts);

            boxID.Text = (IMS.NextProductID + 1).ToString();

            CurrentAssociatedParts = new List<Part>();
        }

        private void OnFormClosing(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var selectedPart = AllCandidateParts.Find(p => p.PartID == int.Parse(CandidateParts.SelectedItems[0].SubItems[0].Text));
            CurrentAssociatedParts.Add(selectedPart);

            IMS.UpdateDataSource(ref AssociatedParts, CurrentAssociatedParts);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (CurrentAssociatedParts.Count != 0)
            {
                var selectedPart = CurrentAssociatedParts.Find(p => p.PartID == int.Parse(AssociatedParts.SelectedItems[0].SubItems[0].Text));
                CurrentAssociatedParts.Remove(selectedPart);
            }
            IMS.UpdateDataSource(ref AssociatedParts, CurrentAssociatedParts);
            AssociatedParts.Refresh();
            AssociatedParts.Update();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Product product = new Product(boxName.Text, decimal.Parse(boxPriceCost.Text), 
                int.Parse(boxInventory.Text), int.Parse(boxMin.Text), int.Parse(boxMax.Text));
            var parts = new List<Part>();
            foreach (ListViewItem item in AssociatedParts.Items)
            {
                parts.Add(AllCandidateParts.Find(p => p.PartID == int.Parse(item.SubItems[0].Text)));
            }

            product.AssociatedParts = parts;

            Controller.inventory.Products.Add(product);

            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            var result =
                from value in AllCandidateParts
                where value.Name.ToLower().Contains(txtSearch.Text.ToLower())
                select value;

            var results = result.ToList();

            CandidateParts.Clear();
            CandidateParts.Columns.Add("Part ID");
            CandidateParts.Columns.Add("Name");
            CandidateParts.Columns.Add("Price");
            CandidateParts.Columns.Add("Inventory");
            CandidateParts.Columns.Add("Min");
            CandidateParts.Columns.Add("Max");
            CandidateParts.View = View.Details;
            var items = new List<ListViewItem>();

            foreach (var part in results)
            {
                items.Add(IMS.CreateListViewItem(part));
            }

            foreach (var item in items)
            {
                CandidateParts.Items.Add(item);
            }
            foreach (var item in items)
            {
                CandidateParts.Items.Add(item);
            }

            CandidateParts.Refresh();
            CandidateParts.Update();
        }
    }
}
