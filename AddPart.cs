﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BFM1_Task
{
    public partial class AddPart : Form
    {
        public AddPart()
        {
            InitializeComponent();
            boxID.Text = (IMS.NextPartID + 1).ToString();
        }

        private void OnFormClosing(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (radInHouse.Checked && !radOutsourced.Checked)
            {
                var part = new InHouse(boxName.Text,
                    decimal.Parse(boxPriceCost.Text),
                    int.Parse(boxInventory.Text),
                    int.Parse(boxMin.Text),
                    int.Parse(boxMax.Text),
                    int.Parse(boxMachineCompany.Text));

                Controller.inventory.AddPart(part);
            }
            else if (!radInHouse.Checked && radOutsourced.Checked)
            {
                var part = new Outsourced(boxName.Text,
                    decimal.Parse(boxPriceCost.Text),
                    int.Parse(boxInventory.Text),
                    int.Parse(boxMin.Text),
                    int.Parse(boxMax.Text),
                    boxMachineCompany.Text);

                Controller.inventory.AddPart(part);
            }
            this.Close();
        }

        private void radOutsourced_CheckedChanged(object sender, EventArgs e)
        {
            if (radInHouse.Checked)
            {
                lblMachineCompany.Text = "Machine ID";
            }
            else if (radOutsourced.Checked)
            {
                lblMachineCompany.Text = "Company Name";
            }
        }
    }
}
