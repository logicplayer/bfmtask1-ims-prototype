﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BFM1_Task
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            // Sample Data
            Controller.inventory.AllParts.Add(new InHouse("Wrench", 4.99M, 2, 0, 1000, 1));
            Controller.inventory.AllParts.Add(new Outsourced("Washer", 0.20M, 100, 0, 2000, "Angry Monkeys Co."));
            Controller.inventory.AllParts.Add(new InHouse("Screw", 0.15M, 40, 0, 1500, 2));

            Controller.inventory.Products.Add(new Product("Screwball", 5.99M, 400, 0, 500));
            Controller.inventory.Products[0].AddAssociatedPart(Controller.inventory.AllParts[1]);
            Controller.inventory.Products[0].AddAssociatedPart(Controller.inventory.AllParts[2]);

            Controller.inventory.Products.Add(new Product("Washing Wrench", 10M, 100, 0, 500));
            Controller.inventory.Products[1].AddAssociatedPart(Controller.inventory.AllParts[0]);
            Controller.inventory.Products[1].AddAssociatedPart(Controller.inventory.AllParts[1]);

            IMS.UpdateDataSource(ref PartsBox, Controller.inventory.AllParts);

            IMS.UpdateDataSource(ref ProductsBox, Controller.inventory.Products);
        }

        private void AddParts_Click(object sender, EventArgs e)
        {
            AddPart addPart = new AddPart();
            addPart.FormClosed += Other_FormClosed;
            this.Hide();
            addPart.Show();
        }

        private void ModifyParts_Click(object sender, EventArgs e)
        {
            Part selectedPart = Controller.inventory.AllParts.Find(p => p.PartID == int.Parse(PartsBox.SelectedItems[0].SubItems[0].Text));
            ModifyPart modifyPart = new ModifyPart(ref selectedPart);
            modifyPart.FormClosed += Other_FormClosed;
            this.Hide();
            modifyPart.Show();
        }

        private void DeleteParts_Click(object sender, EventArgs e)
        {
            var partToDelete = Controller.inventory.AllParts.Find(p => p.PartID == int.Parse(PartsBox.SelectedItems[0].SubItems[0].Text));
            var result = MessageBox.Show($"Are you sure you want to delete {partToDelete.Name}?",
                "Confirm Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                Controller.inventory.DeletePart(partToDelete);
            }
            IMS.UpdateDataSource(ref PartsBox, Controller.inventory.AllParts);
        }

        private void AddProducts_Click(object sender, EventArgs e)
        {
            AddProduct addProduct = new AddProduct();
            addProduct.FormClosed += Other_FormClosed;
            this.Hide();
            addProduct.Show();
        }

        private void ModifyProducts_Click(object sender, EventArgs e)
        {
            var selectedProduct = Controller.inventory.Products.Find(p => p.ProductID == int.Parse(ProductsBox.SelectedItems[0].SubItems[0].Text));
            ModifyProduct modifyProduct = new ModifyProduct(ref selectedProduct);
            modifyProduct.FormClosed += Other_FormClosed;
            this.Hide();
            modifyProduct.Show();
        }

        private void DeleteProducts_Click(object sender, EventArgs e)
        {
            var productToDelete = Controller.inventory.Products.Find(p => p.ProductID == int.Parse(ProductsBox.SelectedItems[0].SubItems[0].Text));
            if (productToDelete.AssociatedParts.Count == 0)
            {
                var result = MessageBox.Show($"Are you sure you want to delete {productToDelete.Name}?",
                    "Confirm Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    Controller.inventory.RemoveProduct(productToDelete.ProductID);
                }
            }
            else
            {
                MessageBox.Show($"Cannot delete {productToDelete.Name} because it has parts associated with it." +
                    " Please remove the associated parts before deleting it", "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }

            IMS.UpdateDataSource(ref ProductsBox, Controller.inventory.Products);
        }

        private void btnPartSearch_Click(object sender, EventArgs e)
        {
            var result =
                from value in Controller.inventory.AllParts
                where value.Name.ToLower().Contains(PartSearch.Text.ToLower())
                select value.PartID;

            var resultIDs = result.ToList();

            var parts = new List<Part>();
            foreach (var resultID in resultIDs)
            {
                var searchResult = Controller.inventory.LookupPart(resultID);
                parts.Add(searchResult);
            }

            IMS.UpdateDataSource(ref PartsBox, parts);
        }

        private void btnProductSearch_Click(object sender, EventArgs e)
        {
            var result =
                from value in Controller.inventory.Products
                let searchString = ProductSearch.Text.ToLower()
                where value.Name.ToLower().Contains(searchString)
                select value.ProductID;

            var resultIDs = result.ToList();
            var products = new List<Product>();
            foreach (var resultID in resultIDs)
            {
                var searchResult = Controller.inventory.LookupProduct(resultID);
                products.Add(searchResult);
            }

            IMS.UpdateDataSource(ref ProductsBox, products);
        }

        private void Other_FormClosed(object sender, FormClosedEventArgs e)
        {
            Reset();
            this.Show();
        }

        public void UpdateDataSource(List<Part> parts)
        {
            PartsBox.Clear();
            PartsBox.Columns.Add("Part ID");
            PartsBox.Columns.Add("Name");
            PartsBox.Columns.Add("Price");
            PartsBox.Columns.Add("Inventory");
            PartsBox.Columns.Add("Min");
            PartsBox.Columns.Add("Max");
            PartsBox.View = View.Details;
            var items = new List<ListViewItem>();

            foreach (var part in parts)
            {
                items.Add(IMS.CreateListViewItem(part));
            }

            foreach (var item in items)
            {
                PartsBox.Items.Add(item);
            }

            PartsBox.Refresh();
            PartsBox.Update();
        }

        public void Reset()
        {
            IMS.UpdateDataSource(ref PartsBox, Controller.inventory.AllParts);
            IMS.UpdateDataSource(ref ProductsBox, Controller.inventory.Products);
            PartSearch.Text = "";
            ProductSearch.Text = "";
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
